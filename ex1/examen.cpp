#include <iostream>
#include <cstdlib>
#include <cmath>
#include <vector>

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "shader_utils.h"

#include <cstdio>
#include <math.h>

GLuint vbo_surface;
GLuint vbo_color;
GLint uniform_mvp;

GLuint program;

GLint attribute_coord3d;
GLint attribute_color;

GLfloat* surface_vertices;
GLfloat* surface_color;

int screen_width = 800, screen_height = 800;

int number_of_steps = 100;
int factorial[4] = {1, 1, 2, 6};

float binomial(int n, int i) {
    return factorial[n] / (factorial[i] * factorial[n - i]) * 1.0;
}

float bernstein(int m, int i, float t) {
    return binomial(m, i) * powf(t, i) * powf(1 - t, m - i);
}

void print(std::string s) {
    std::cout << s << std::endl;
}

std::vector<float> buildBezier(GLfloat control_points[4][4][3]) {
    float u = 0.0, v = 0.0;
    float x, y, z;
    float B_i, B_j;

    std::vector<float> ps;

    for (int i = 0; i <= number_of_steps; i++) { // recorrer el par�metro u
        v = 0.0;
        for (int j = 0; j <= number_of_steps; j++) { // recorrer el par�metro v
            
            x = y = z = 0.0;

            // para la sumatoria anidada en la formula de Bezier
            for (int k = 0; k < 4; k++) { // sumatoria en f�rmula de Bezier
                B_i = bernstein(3, k, u);
                for (int l = 0; l < 4; l++) { // sumatoria en f�rmula de Bezier
                    B_j = bernstein(3, l, v);

                    x += B_i * B_j * control_points[k][l][0];
                    y += B_i * B_j * control_points[k][l][1];
                    z += B_i * B_j * control_points[k][l][2];
                }
            }

            ps.push_back(x);
            ps.push_back(y);
            ps.push_back(z);
            
            v += 1.0 / number_of_steps;
        }

        u += 1.0 / number_of_steps;
    }

    return ps;
}

typedef struct punto {
    float x;
    float y;
    float z;
} Punto;

int n_points;

std::vector<GLfloat> leerArchivo(const char* filename){
    FILE* file = fopen(filename, "rt");

    // leer cantidad puntos de control y parches
    int n_puntos_control, n_superficies_control;
    fscanf(file, "%d %d", &n_puntos_control, &n_superficies_control);
    
    std::vector<Punto> puntos_control (n_puntos_control);

    // leer puntos de control
    float x, y, z;
    for (int i = 0; i < n_puntos_control; i++) {
        fscanf(file, "%f %f %f", &x, &y, &z);
        Punto p = {x, y, z};
        puntos_control[i] = p;
    }

    // vector para guardar todos los puntos de las superficies de bezier calculados
    std::vector<GLfloat> all_points;

    GLfloat cp[4][4][3];
    int indices_puntos_control[16];
    for (int i = 0; i < n_superficies_control; i++) {

        // leer indices de puntos de control
        for (int p = 0; p < 16; p++) {
            fscanf(file, "%d", &(indices_puntos_control[p]));
        }

        // obtener puntos de control segun �ndices
        for (int p = 0; p < 16; p++) {
            cp[p / 4][p % 4][0] = puntos_control[indices_puntos_control[p]].x;
            cp[p / 4][p % 4][1] = puntos_control[indices_puntos_control[p]].y;
            cp[p / 4][p % 4][2] = puntos_control[indices_puntos_control[p]].z;
        }

        // construir superficie de bezier
        std::vector<float> ps = buildBezier(cp);
        
        // guardar puntos
        for (int i = 0; i < ps.size(); i++) {
            all_points.push_back(ps[i]);
        }

        std::cout << "calculated surface " << i << " size: " << all_points.size() <<std::endl;
    }

    fclose(file);

    return all_points;

}

bool init_resources(){

    std::vector<GLfloat> all_points = leerArchivo("control.txt");

    surface_vertices = new GLfloat[all_points.size()];
    surface_color = new GLfloat[all_points.size()];

    n_points = all_points.size() / 3;

    for (int i = 0; i < all_points.size(); i++) {
        surface_vertices[i] = all_points[i];
    }

    for (int i = 0; i < all_points.size() / 3; i++) {
        surface_color[3 * i] = 0.0;
        surface_color[3 * i + 1] = 1.0;
        surface_color[3 * i + 2] = 0.0;
    }

    glGenBuffers(1, &vbo_surface);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_surface);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * all_points.size(), surface_vertices, GL_STATIC_DRAW);

    glGenBuffers(1, &vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * all_points.size(), surface_color, GL_STATIC_DRAW);

    GLint link_ok = GL_FALSE;
    GLuint vs, fs;
    if((vs = create_shader("basic3.v.glsl", GL_VERTEX_SHADER))==0) return false;
    if((fs = create_shader("basic3.f.glsl", GL_FRAGMENT_SHADER))==0) return false;

    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &link_ok);
    if(!link_ok){
        std::cout << "Problemas con el Shader" << std::endl;
        return false;
    }

    attribute_coord3d = glGetAttribLocation(program, "coord3d");
    if(attribute_coord3d == -1){
        std::cout << "No se puede asociar el atributo coord3d" << std::endl;
        return false;
    }

    attribute_color = glGetAttribLocation(program, "color");
    if(attribute_color == -1){
        std::cout << "No se puede asociar el atributo color" << std::endl;
        return false;
    }

    uniform_mvp = glGetUniformLocation(program, "mvp");
    if(uniform_mvp == -1){
        std::cout << "No se puede asociar el uniform mvp" << std::endl;
        return false;
    }

    return true;
}

void onDisplay(){
    //Creamos matrices de modelo, vista y proyeccion
    glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, -1.5));
    glm::mat4 view  = glm::lookAt(glm::vec3(4.0f, 4.0f, 4.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    glm::mat4 projection = glm::perspective(45.0f, 1.0f*screen_width/screen_height, 0.1f, 10.0f);
    glm::mat4 mvp = projection * view * model;

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glUseProgram(program);
    //Enviamos la matriz que debe ser usada para cada vertice
    glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, glm::value_ptr(mvp));

    glEnableVertexAttribArray(attribute_coord3d);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_surface);

    glVertexAttribPointer(
        attribute_coord3d,
        3,
        GL_FLOAT,
        GL_FALSE,
        0, 0
    );

    glEnableVertexAttribArray(attribute_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color);

    glVertexAttribPointer(
        attribute_color,
        3,
        GL_FLOAT,
        GL_FALSE,
        0, 0
    );

    glBindBuffer(GL_ARRAY_BUFFER, vbo_surface);

    glDrawArrays(GL_POINTS, 0, n_points);

    glDisableVertexAttribArray(attribute_coord3d);
    glDisableVertexAttribArray(attribute_color);
    glutSwapBuffers();
}

void onReshape(int w, int h){
    screen_width = w;
    screen_height = h;

    glViewport(0,0,screen_width, screen_height);
}

void free_resources(){
    glDeleteProgram(program);
    glDeleteBuffers(1, &vbo_surface);
    glDeleteBuffers(1, &vbo_color);


    /*EXAMEN - OPCIONAL: elimine la memoria que se utiliz� en los buffers. Use delete o free en los arreglos
                surface_vertices y surface_color
    */

    delete[] surface_vertices;
    delete[] surface_color;
}

int main(int argc, char* argv[]){
    glutInit(&argc, argv);
    glutInitContextVersion(2,0);
    glutInitDisplayMode(GLUT_RGBA | GLUT_ALPHA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(screen_width, screen_height);
    glutCreateWindow("OpenGL");

    GLenum glew_status = glewInit();
    if(glew_status != GLEW_OK){
        std::cout << "Error inicializando GLEW" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(!GLEW_VERSION_2_0){
        std::cout << "Tu tarjeta grafica no soporta OpenGL 2.0" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(init_resources()){
        glutDisplayFunc(onDisplay);
        glutReshapeFunc(onReshape);
        glEnable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glutMainLoop();
    }

    free_resources();
    exit(EXIT_SUCCESS);
}
