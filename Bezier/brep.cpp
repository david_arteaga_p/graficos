#include <iostream>
#include <cstdlib>
#include <vector>
#include <cstdio>

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "shader_utils.h"
#include <string>

#include <fstream>

GLuint program;
GLint attribute_coord;

GLint uniform_mvp;
GLint uniform_xmin;
GLint uniform_xmax;
GLint uniform_ymin;
GLint uniform_ymax;
GLint uniform_zmin;
GLint uniform_zmax;

int screen_width = 800, screen_height = 800;

#define MAXV 100000
#define MAXT 1000000

// para poder escalar y traladar la superficie en el espacio
float dx = 0.0;
float scale = 1.0;


// START FOR BEZIER
int number_of_steps = 100;
int numPoints;

int factorial[4] = {1, 1, 2, 6};

float binomial(int n, int i) {
    return factorial[n] / (factorial[i] * factorial[n - i]) * 1.0;
}

float bernstein(int m, int i, float t) {
    return binomial(m, i) * powf(t, i) * powf(1 - t, m - i);
}

GLfloat cp[4][4][3] = {
    {{0.0, 0.0, 1.0},
     {0.0, 0.0, 0.66},
     {0.0, 0.0, 0.33},
     {0.0, 0.0, 0.0}},

    {{0.33, 0.0, 1.0},
     {0.33, 1.5, 0.66},
     {0.33, 1.5, 0.33},
     {0.33, 0.0, 0.0}},

    {{0.66, 0.0, 1.0},
     {0.66, 1.5, 0.66},
     {0.66, 1.5, 0.33},
     {0.66, 0.0, 0.0}},

    {{1.0, 0.0, 1.0},
     {1.0, 0.0, 0.66},
     {1.0, 0.0, 0.33},
     {1.0, 0.0, 0.0}}
};

// END FOR BEZIER

float xmin=1e09, xmax=-1e09, ymin=1e09, ymax=-1e09, zmin=1e09, zmax=-1e09;

struct Surface {
  GLuint vbo_object;
  GLuint ibo_object;

  GLfloat vertices[MAXV][3];
  GLushort triangles[MAXT][3];

  int numVertices;
  int numTriangles;
};

Surface surface;

bool init_resources(){
    // START BEZIER
    float u = 0.0, v = 0.0;
    float x, y, z;
    float B_i, B_j;
    std::vector<float> ps; 
    int cc = 0;
    for (int i = 0; i <= number_of_steps; i++) { // recorrer el parámetro u
        v = 0.0;
        for (int j = 0; j <= number_of_steps; j++) { // recorrer el parámetro v
            
            x = y = z = 0.0;

            // para la sumatoria anidada en la formula de Bezier
            for (int k = 0; k < 4; k++) { // sumatoria en fórmula de Bezier
                B_i = bernstein(3, k, u);
                for (int l = 0; l < 4; l++) { // sumatoria en fórmula de Bezier
                    B_j = bernstein(3, l, v);

                    x += B_i * B_j * cp[k][l][0];
                    y += B_i * B_j * cp[k][l][1];
                    z += B_i * B_j * cp[k][l][2];
                }
            }

            // guardar coordenadas de puntos
            ps.push_back(x);
            ps.push_back(y);
            ps.push_back(z);

            v += 1.0 / number_of_steps;
        }

        u += 1.0 / number_of_steps;
    }

    numPoints = ps.size() / 3;

    surface.numVertices = numPoints;
    surface.numTriangles = (int)(pow(sqrt(numPoints) - 1, 2) * 2);
    
    // WRITE OFF FILE
    std::ofstream f ("superficie.off");
    f << "OFF\n";
    f << surface.numVertices << " " << surface.numTriangles << " 0\n";

    // fill vertices for surface
    for (int ver = 0; ver < surface.numVertices; ver++) {
        GLfloat a = surface.vertices[ver][0] = ps[ver * 3 + 0];
        GLfloat b = surface.vertices[ver][1] = ps[ver * 3 + 1];
        GLfloat c = surface.vertices[ver][2] = ps[ver * 3 + 2];

        f << a << " " << b << " " << c << std::endl;

        // obtener puntos mínimos y máximos
        if(surface.vertices[ver][0] < xmin)   xmin = surface.vertices[ver][0];
        if(surface.vertices[ver][0] > xmax)   xmax = surface.vertices[ver][0];
        if(surface.vertices[ver][1] < ymin)   ymin = surface.vertices[ver][1];
        if(surface.vertices[ver][1] > ymax)   ymax = surface.vertices[ver][1];
        if(surface.vertices[ver][2] < zmin)   zmin = surface.vertices[ver][2];
        if(surface.vertices[ver][2] > zmax)   zmax = surface.vertices[ver][2];
    }
    
    // recorrer la matriz de puntos columna por columna y guardar los indices de los puntos que formarán los triángulos
    long t = 0;
    long sqrt_numPoints = sqrt(numPoints);
    for (int x = 0; x < sqrt_numPoints - 1; x++) {
        for (int y = 0; y < sqrt_numPoints - 1; y++) {
            // agarrar puntos de 3 en 3, y formar un triángulo con ellos
            GLushort a = surface.triangles[t][0] = x * sqrt_numPoints + y;
            GLushort b = surface.triangles[t][1] = x * sqrt_numPoints + y + 1;
            GLushort c = surface.triangles[t][2] = (x + 1) * sqrt_numPoints + y;
            f << "3 " << a << " " << b << " " << c << std::endl;
            t++;

            // agarrar puntos de 3 en 3, y formar un triángulo con ellos
            a = surface.triangles[t][0] = x * sqrt_numPoints + y + 1;
            b = surface.triangles[t][1] = (x + 1) * sqrt_numPoints + y;
            c = surface.triangles[t][2] = (x + 1) * sqrt_numPoints + y + 1;
            f << "3 " << a << " " << b << " " << c << std::endl;
            t++;

        }
    }
    // END BEZIER

    f.close();


    //read_file("NR0.off", surface);

    // crear buffer para coordenadas
    glGenBuffers(1, &surface.vbo_object); // crear buffer en gpu
    glBindBuffer(GL_ARRAY_BUFFER, surface.vbo_object); // pedir autorización para usar buffer (buffer de float)
    glBufferData(GL_ARRAY_BUFFER, surface.numVertices * 3 * sizeof(GLfloat), surface.vertices, GL_STATIC_DRAW); // enviar data desde RAM a GPU

    // crear buffer para triangulos
    glGenBuffers(1, &surface.ibo_object);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, surface.ibo_object); // tipo de buffer -> unsigned int; para almacenar triangulos
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                surface.numTriangles * 3 * sizeof(GLushort), surface.triangles, GL_STATIC_DRAW);


    // cargar shaders
    GLint link_ok = GL_FALSE;
    GLuint vs, fs;
    if((vs = create_shader("basic.v.glsl", GL_VERTEX_SHADER))==0) return false;
    if((fs = create_shader("basic.f.glsl", GL_FRAGMENT_SHADER))==0) return false;

    // compilar shaders
    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    // verificar que se compiló bien
    glGetProgramiv(program, GL_LINK_STATUS, &link_ok);
    if(!link_ok){
        std::cout << "Problemas con el Shader" << std::endl;
        return false;
    }

    // extraer una referencia a los shaders, a los programas del GPU
    attribute_coord = glGetAttribLocation(program, "coord");
    if(attribute_coord == -1){
        std::cout << "No se puede asociar el atributo coord" << std::endl;
        return false;
    }

    uniform_mvp = glGetUniformLocation(program, "mvp");
    if(uniform_mvp == -1){
        std::cout << "No se puede asociar el uniform mvp" << std::endl;
        return false;
    }


    uniform_xmax = glGetUniformLocation(program, "xmax");
    if(uniform_xmax == -1){
        std::cout << "No se puede asociar el uniform xmax" << std::endl;
        return false;
    }

    uniform_xmin = glGetUniformLocation(program, "xmin");
    if(uniform_xmin == -1){
        std::cout << "No se puede asociar el uniform xmin" << std::endl;
        return false;
    }

    uniform_ymax = glGetUniformLocation(program, "ymax");
    if(uniform_ymax == -1){
        std::cout << "No se puede asociar el uniform ymax" << std::endl;
        return false;
    }

    uniform_ymin = glGetUniformLocation(program, "ymin");
    if(uniform_ymin == -1){
        std::cout << "No se puede asociar el uniform ymin" << std::endl;
        return false;
    }

    uniform_zmax = glGetUniformLocation(program, "zmax");
    if(uniform_zmax == -1){
        std::cout << "No se puede asociar el uniform zmax" << std::endl;
        return false;
    }

    uniform_zmin = glGetUniformLocation(program, "zmin");
    if(uniform_zmin == -1){
        std::cout << "No se puede asociar el uniform zmin" << std::endl;
        return false;
    }

    return true;
}

void onDisplay(){
    //Creamos matrices de modelo, vista y proyeccion
    glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(dx, 0, 0));

    glm::mat4 ss = glm::scale(glm::mat4(1.0), glm::vec3(scale, scale, scale));
    //Creamos matrices de modelo, vista y proyeccion
    glm::mat4 view  = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(-3.5f, 1.0f, 3.0f)); // para simular camara, donde está, hacia donde mira, dirección del eje superior de la camara
    glm::mat4 projection = glm::perspective(45.0f, 1.0f*screen_width/screen_height, 0.1f, 10.0f); // definir como el objeto se ve en la camara -> proyecta puntos en 3d a la pantalla
    glm::mat4 mvp = projection * view * model * ss;

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(program);

    //Enviamos la matriz que debe ser usada para cada vertice
    glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, glm::value_ptr(mvp));
    glUniform1f(uniform_xmax, xmax);
    glUniform1f(uniform_xmin, xmin);
    glUniform1f(uniform_ymax, ymax);
    glUniform1f(uniform_ymin, ymin);
    glUniform1f(uniform_zmax, zmax);
    glUniform1f(uniform_zmin, zmin);

    glEnableVertexAttribArray(attribute_coord);
    glBindBuffer(GL_ARRAY_BUFFER, surface.vbo_object);

    glVertexAttribPointer(
        attribute_coord,
        3,
        GL_FLOAT,
        GL_FALSE,
        0, 0
    );

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, surface.ibo_object);
    glDrawElements(GL_TRIANGLES, surface.numTriangles * 3, GL_UNSIGNED_SHORT, 0);

    glDisableVertexAttribArray(attribute_coord);


    glutSwapBuffers();
}

void onReshape(int w, int h){
    screen_width = w;
    screen_height = h;

    glViewport(0,0,screen_width, screen_height);
}

void free_resources(){
    glDeleteProgram(program);
    glDeleteBuffers(1, &surface.vbo_object);
    glDeleteBuffers(1, &surface.ibo_object);
}

void key(unsigned char key, int x, int y) {
    switch(key) {
        case 'd': dx += 0.05; break;
        case 'a': dx -= 0.05; break;
        case 'w': scale += 0.05; break;
        case 's': scale -= 0.05; break;
    }
    glutPostRedisplay();
}

int main(int argc, char* argv[]){
    glutInit(&argc, argv);
    glutInitContextVersion(2,0);
    glutInitDisplayMode(GLUT_RGBA | GLUT_ALPHA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(screen_width, screen_height);
    glutCreateWindow("OpenGL");

    GLenum glew_status = glewInit();
    if(glew_status != GLEW_OK){
        std::cout << "Error inicializando GLEW" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(!GLEW_VERSION_2_0){
        std::cout << "Tu tarjeta grafica no soporta OpenGL 2.0" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(init_resources()){
        glutDisplayFunc(onDisplay);
        glutReshapeFunc(onReshape);
        glutKeyboardFunc(key);
        glEnable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glutMainLoop();
    }

    free_resources();
    exit(EXIT_SUCCESS);
}

/**
Cada objeto necesita lo siguiente:
  - Geometría
  - Buffer de triangulos, buffer de vértices
  - Cada objeto debe dibujarse por separado
** Shaders son independientes del objeto que se está dibujando

https://sketchfab.com/
*/
