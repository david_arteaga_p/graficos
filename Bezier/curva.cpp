#include <iostream>
#include <cstdlib>
#include <cmath>

#include <GL/glew.h>
#include <GL/freeglut.h>
#include "shader_utils.h"

using namespace std;

GLuint vbo_curve;
GLuint vbo_color;

GLuint program;

GLint attribute_coord2d;
GLint attribute_color;


//Puntos de control
GLfloat cp[4][2]={
	{0.0, 0.0}
	,{0.0, 0.5}
	,{0.3, -0.5}
	,{0.5, 0.0}
};

GLfloat cp2[4][2]={
    {cp[3][0], cp[3][1]}
    ,{0.7, 0.5}
    ,{0.8, -0.5}
    ,{0.99, 0.8}
};

float bezier(float tt, float cpp[4][2], int coord) {

    float t1c = 1 - tt;
    float t2c = t1c * t1c;
    float t3c = t2c * t1c;
    float t2 = tt * tt;
    float t3 = t2 * tt;

    return t3c * cpp[0][coord] + 3 * t2c * tt * cpp[1][coord] + 3 * t1c * t2 * cpp[2][coord] + t3 * cpp[3][coord];
}


// number of steps to compute points on the Bezier curve -> compute | step | compute | step | compute
// there will be one more point than the number_of_steps
int number_of_steps = 100; 
float delta_t = 1.0 / number_of_steps;

bool init_resources(){

    // crear arreglos para poner las coordenadas y colores de coordenadas
    GLfloat points[2 * 2 * (number_of_steps + 1)];
    GLfloat colors[2 * 3 * (number_of_steps + 1)];
    float t = 0.0;

    for (int i = 0; i <= number_of_steps; i++) {
        points[2 * i] = bezier(t, cp, 0);
        points[2 * i + 1] = bezier(t, cp, 1);

        colors[3 * i] = 1.0;
        colors[3 * i + 1] = 0.0;
        colors[3 * i + 2] = 0.0;

        t += delta_t;
    }

    t = 0.0;
    for (int i = 0; i <= number_of_steps; i++) {
        points[2 * (number_of_steps + 1) + 2 * i] = bezier(t, cp2, 0);
        points[2 * (number_of_steps + 1) + 2 * i + 1] = bezier(t, cp2, 1);

        colors[3 * (number_of_steps + 1) + 3 * i] = 1.0;
        colors[3 * (number_of_steps + 1) + 3 * i + 1] = 0.0;
        colors[3 * (number_of_steps + 1) + 3 * i + 2] = 0.0;

        t += delta_t;
    }

    glGenBuffers(1, &vbo_curve);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_curve);
    glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

    glGenBuffers(1, &vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

	GLint link_ok = GL_FALSE;
    GLuint vs, fs;
    if((vs = create_shader("basic.v.glsl", GL_VERTEX_SHADER))==0) return false;
    if((fs = create_shader("basic.f.glsl", GL_FRAGMENT_SHADER))==0) return false;

    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &link_ok);
    if(!link_ok){
        std::cout << "Problemas con el Shader" << std::endl;
        return false;
    }

    attribute_coord2d = glGetAttribLocation(program, "coord2d");
    if(attribute_coord2d == -1){
        std::cout << "No se puede asociar el atributo coord2d" << std::endl;
        return false;
    }

    attribute_color = glGetAttribLocation(program, "color");
    if(attribute_color == -1){
        std::cout << "No se puede asociar el atributo color" << std::endl;
        return false;
    }

    return true;
}

void onDisplay(){
	glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(program);

    // dibujar puntos

    // entre cada par de puntos, dibujar una línea

    // habilito atributo con glEnableVertexAttribArray obtiene datos de vertex del GL_ARRAY_BUFFER
    // bind buffer de donde se obtendrá vertex points a GL_ARRAY_BUFFER
    // configurar como se tomarán datos del buffer actualmente binded a GL_ARRAY_BUFFER

    glEnableVertexAttribArray(attribute_coord2d); // habilitar el atributo coordenada 2d del shader para poder usarlo
    glBindBuffer(GL_ARRAY_BUFFER, vbo_curve); // bind the array for vertex coordinates to the array that holds the curve's points
    glVertexAttribPointer(attribute_coord2d // atributo donde voy a utilizar los datos
        ,2 // cada dos coordenadas es un punto (cada cuantos valores le pertenecen a UN punto)
        ,GL_FLOAT // tipo de dato de los datos del buffer
        ,GL_FALSE // hay datos mezclados
        ,0, 0);

    glEnableVertexAttribArray(vbo_curve);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color);
    glVertexAttribPointer(attribute_color
        ,3
        ,GL_FLOAT
        ,GL_FALSE
        ,0, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_curve);
    glDrawArrays(GL_LINE_STRIP, 0, 2 * (number_of_steps + 1)); // dibujar primitivas: puntos, líneas, etc.

    glDisableVertexAttribArray(attribute_coord2d);
    glDisableVertexAttribArray(attribute_color);

    glutSwapBuffers();
}

void free_resources(){
	glDeleteProgram(program);
    glDeleteBuffers(1, &vbo_curve);
    glDeleteBuffers(1, &vbo_color);
}

int main(int argc, char* argv[]){
    glutInit(&argc, argv);
    glutInitContextVersion(2,0);
    glutInitDisplayMode(GLUT_RGBA | GLUT_ALPHA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(512, 512);
    glutCreateWindow("OpenGL");

    GLenum glew_status = glewInit();
    if(glew_status != GLEW_OK){
        std::cout << "Error inicializando GLEW" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(!GLEW_VERSION_2_0){
        std::cout << "Tu tarjeta grafica no soporta OpenGL 2.0" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(init_resources()){
        glutDisplayFunc(onDisplay);
        glEnable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glutMainLoop();
    }

    free_resources();
    exit(EXIT_SUCCESS);
}
