#version 450

in vec2 coord2d;
uniform float t;
void main(void){
    vec2 new_coord = coord2d + t; //todos los puntos se mueven
    gl_Position = vec4(new_coord, 0, 1);//indica al gpu que será 2d. Calcula posiciones
    //  gl_Position = vec4(new_cord, 0, 1); estaticos
}
